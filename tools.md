# Tools

 

## Analytics, Analysis, Investigation and Defence Tools.

 

**RITA**:  *Real Intelligence Threat Analytics* 

RITA is an open source framework for network traffic analysis. 

https://github.com/ocmdev/rita

 

 

**BRO**: *Network Security Monitor

Bro is a powerful network analysis framework that is much different from the typical IDS you may know. 

https://www.bro.org/

 

**GRR**: *Google Rapid Response* 

GRR: Rapid Response and incident reponse framework for live forensics. 

https://github.com/google/grr

 

 

**KANSA**: *Incident repsonse framework* 

A modular incident response framework in Powershell. It's been tested in PSv2 / .NET 2 and later and works mostly without issue. 

https://github.com/davehull/Kansa

 

 

**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**

 

## OSSINT and Recon

 

Open Source Intelligence and Reconnaissance

 

**NAMECHK**: *namechk.com* 

A URL and Social Media Username Checker.

https://namechk.com/

 

 

**Pushpin**: *Social media geo tagging ossint*

Pushpin modules broken out of recon-ng and turned into a webapp. Pushpin-web provides an easy web interface to keep track of geotagged social media activity. 

https://github.com/DakotaNelson/pushpin-web

 

 

**Search Diggity**: *Google (and other search engines) hacking* 

Allowing you to find information disclosures and exposed vulnerabilities before others do.  

https://www.bishopfox.com/resources/tools/google-hacking-diggity/attack-tools/

 

 

**Maltego**: *Interactive Data Mining Tool* 

A data mining tool that renders directed graphs for link analysis. 

https://www.paterva.com/

 

 

**EnCase**: *Forensic Investigations* 

Paid forensic investigation toolset 

https://www.guidancesoftware.com/encase-forensic

 

 

**SleuthKit**: *Open Source Digital Forensics* 

*Open Source Digital Forensics - Autopsy and The Sleuthkit* 

https://www.sleuthkit.org/

 

 

 

**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**

 

### Attack/Penetration Testing

 

**ISR - Evilgrade**:  *Fake Update Injection* 

Evilgrade is a modular framework that allows the user to take advantage of poor upgrade implementations by injecting fake updates. 

https://github.com/infobyte/evilgrade

 

 

**WarVox** *War Dialling* 

An open source War Dialler 

https://github.com/rapid7/warvox

 

 

**Kismet**: *An opensource wireless detection and sniffing tool* 

Kismet is a wireless network detector, sniffer, and intrusion detection system. Kismet works predominately with Wi-Fi (IEEE 802.11) networks, but can be expanded via plug-ins to handle other network types. 

https://www.kismetwireless.net/

 

 

**Konboot**: *Windows Login Bypass*   

Kon-Boot is an application which will silently bypass the authentication process of Windows based operating systems. 

http://www.piotrbania.com/all/kon-boot/

 

 

**Inception**: *Physical memory manipulation and hacking tool* 

Inception is a physical memory manipulation and hacking tool exploiting PCI-based DMA. The tool can attack over FireWire, Thunderbolt, ExpressCard, PC Card and any other PCI/PCIe HW interfaces. 

https://github.com/carmaa/inception

 

 

**Hak5 Hardware**: *Lan Turtle, BashBunny, Rubberduck, Pineapple* 

Hak5 sell a list of hardware for penetration testing and red team scenarios 

https://www.hak5.org/

 

 

**Responder**: *LLMNR/NBT-NS/DNS Poisoner*  

Responder (hash capture on network) 

https://github.com/SpiderLabs/Responder

 

 

**Bettercap**: *Network attacks and monitoring* 

Bettercap - "The swiss army knife of network attack and monitoring tools" 

https://www.bettercap.org/

 

 

**Powerbleed**: *OpenSSL Heartbleed Checker*

Powerbleed is a powershell module that allows you to check for the OpenSSL heartbeat vulnerability, exploit it and write data to a file if desired

https://bitbucket.org/jsthyer/powerbleed

 

 

**Pacdoor**: *POC JS Malware via proxy pacs*  

Pacdoor is a proof-of-concept JavaScript malware implemented as a Proxy Auto-Configuration (PAC) File. 

https://github.com/SafeBreach-Labs/pacdoor



**Ruler** *Outlook/Exchange Services attacks* 

Ruler is a tool that allows you to interact with Exchange servers remotely, through either the MAPI/HTTP or RPC/HTTP protocol. The main aim is abuse the client-side Outlook features and gain a shell remotely.

https://github.com/sensepost/ruler


**WPScan** *Word Press Scan* 

Wordpress Vulnerability Scanner

https://github.com/wpscanteam/wpscan


**Invoke-Phant0m** *Event Log Killer*  

This script walks thread stacks of Event Log Service process (spesific svchost.exe) and identify Event Log Threads to kill Event Log Service Threads

https://github.com/hlldz/Invoke-Phant0m

**Expired Domains** *recently expired domains* 

expireddomains.net - get expired domain categorised as "Health" for example for a proxy, copy site off archive.org then set up user agents to redirect something like powershell empire but load "Normal" website for normal ssl 

https://expireddomains.net

